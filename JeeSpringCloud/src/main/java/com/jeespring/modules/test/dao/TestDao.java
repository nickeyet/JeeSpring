/**
 * Copyright &copy; 2012-2016 <a href="https://git.oschina.net/guanshijiehnan/JeeRTD">JeeSite</a> All rights reserved.
 */
package com.jeespring.modules.test.dao;

import com.jeespring.modules.test.entity.Test;
import com.jeespring.common.persistence.InterfaceBaseDao;
import org.apache.ibatis.annotations.Mapper;

/**
 * 测试DAO接口
 * @author 黄炳桂 516821420@qq.com
 * @version 2013-10-17
 */
@Mapper
public interface TestDao extends InterfaceBaseDao<Test> {
	
}
