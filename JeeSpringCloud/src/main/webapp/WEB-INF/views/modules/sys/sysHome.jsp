<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>首页</title>
    <meta name="decorator" content="default"/>
    <%@ include file="/WEB-INF/views/include/head.jsp"%>
    <script type="text/javascript">
        $(document).ready(function() {
            WinMove();
        });
    </script>
</head>
<body class="gray-bg">
</div>
<div class="row  border-bottom white-bg dashboard-header">
    <div class="col-sm-12">
        <blockquote class="text-info" style="font-size:14px">
            ${indexSysConfig.description}
                JeeSpring开发平台采用 SpringBoot+Redis+ActiveMQ+SpringMVC + MyBatis + BootStrap + Apache Shiro + Ehcache 开发组件的基础架构.
                JeeSpring基于SpringBoot+SpringMVC+Mybatis+Redis+SpringCloud微服务分布式代码生成的敏捷开发系统架构。项目代码简洁,注释丰富,上手容易,还同时集中分布式、微服务,同时包含许多基础模块(用户管理,角色管理,部门管理,字典管理等10个模块。成为大众认同、大众参与、成就大众、大众分享的开发平台。JeeSpring官方qq群(328910546)。代码生成前端界面、底层代码（spring mvc、mybatis、Spring boot、Spring Cloud、微服务的生成）、安全框架、视图框架、服务端验证、任务调度、持久层框架、数据库连接池、缓存框架、日志管理、IM等核心技术。努力用心为大中小型企业打造全方位J2EE企业级平台ORM/Redis/Service仓库开发解决方案。一个RepositoryService仓库就直接实现dubbo、微服务、基础服务器对接接口和实现。
                努力用心为大中小型企业打造全方位J2EE企业级平台开发解决方案。
                Spring Boot/Spring cloud微服务是利用云平台开发企业应用程序的最新技术，它是小型、轻量和过程驱动的组件。微服务适合设计可扩展、易于维护的应用程序。它可以使开发更容易，还能使资源得到最佳利用。
        </blockquote>

        <hr>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>平台视频介绍</h5>
                    <div style="text-align: center;font-size: 14px;color: #666;">${oaNotify.title}</div><br/>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="index.html#"></a>
                            </li>
                            <li><a href="index.html#"></a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <video id="vid1" loop="loop"  width="100%" height="100%" onended="" autoplay="autoplay" muted="muted" autobuffer="autobuffer" preload="auto" oncontextmenu="return false" data-hasaudio=""
                           style="background-color: white;opacity: 1;visibility: visible;height: 100%;width: 100%;object-fit:cover;object-position: center center;"
                           src="../static/common/login/images/flat-avatar1.mp4"></video>
                    <video id="vid4" loop="loop"  width="100%" height="100%" onended="" autoplay="autoplay" muted="muted" autobuffer="autobuffer" preload="auto" oncontextmenu="return false" data-hasaudio=""
                           style="background-color: white;opacity: 1;visibility: visible;height: 100%;width: 100%;object-fit:cover;object-position: center center;"
                           src="../static/common/login/images/flat-avatar3.mp4"></video>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>平台视频部署</h5>
                    <div style="text-align: center;font-size: 14px;color: #666;">${oaNotify.title}</div><br/>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="index.html#"></a>
                            </li>
                            <li><a href="index.html#"></a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <video id="vid2" loop="loop" width="100%" height="100%" onended="" autoplay="autoplay" muted="muted" autobuffer="autobuffer" preload="auto" oncontextmenu="return false" data-hasaudio=""
                           style="background-color: white;opacity: 1;visibility: visible;height: 100%;width: 100%;object-fit:cover;object-position: center center;"
                           src="../static/common/login/images/flat-avatar11.mp4"></video>
                    <video id="vid13" loop="loop" width="100%" height="100%" onended="" autoplay="autoplay" muted="muted" autobuffer="autobuffer" preload="auto" oncontextmenu="return false" data-hasaudio=""
                           style="background-color: white;opacity: 1;visibility: visible;height: 100%;width: 100%;object-fit:cover;object-position: center center;"
                           src="../static/common/login/images/flat-avatar13.mp4"></video>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>平台视频教程</h5>
                    <div style="text-align: center;font-size: 14px;color: #666;">${oaNotify.title}</div><br/>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="index.html#"></a>
                            </li>
                            <li><a href="index.html#"></a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <video id="vid3" loop="loop" width="100%" height="100%" onended="" autoplay="autoplay" muted="muted" autobuffer="autobuffer" preload="auto" oncontextmenu="return false" data-hasaudio=""
                           style="background-color: white;opacity: 1;visibility: visible;height: 100%;width: 100%;object-fit:cover;object-position: center center;"
                           src="../static/common/login/images/flat-avatar12.mp4"></video>
                    <img width="100%" height="auto" src="../static/common/login/images/football.gif">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-sm-4">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>公告通知</h5>
                    <div style="text-align: center;font-size: 14px;color: #666;">${oaNotify.title}</div><br/>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="index.html#"></a>
                            </li>
                            <li><a href="index.html#"></a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
                        <ol>
                            <c:forEach items="${pageOaNotify.list}" var="oaNotify">
                                <li>${fns:abbr(oaNotify.title,50)}         ${fns:getDictLabel(oaNotify.type, 'oa_notify_type', '')}
                                    <fmt:formatDate value="${oaNotify.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/></li></br>
                            </c:forEach>
                        </ol>
                        <hr>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>升级日志</h5> <span class="label label-primary">K+</span>
                    <div class="ibox-tools">
                        <!a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="index.html#"></a>
                            </li>
                            <li><a href="index.html#"></a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="panel-body">
                        <div class="panel-group" id="version">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#version" href="#v1.0"></a><code class="pull-right"></code>
                                    </h5>
                                </div>
                                <div id="v2.0" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <ol>
                                            <c:forEach items="${pageOaNotifyLog.list}" var="oaNotify">
                                                <li>${fns:abbr(oaNotify.title,50)}         ${fns:getDictLabel(oaNotify.type, 'oa_notify_type', '')}
                                                    <fmt:formatDate value="${oaNotify.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/></li></br>
                                            </c:forEach>
                                        </ol>
                                        <hr>
                                        ${indexTopicsShowList.description}
                                    </div>
                                </div>
                            </div>
                            <!--div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.9</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.8</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.7</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.6</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.5</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.4</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.3</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                             <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.2</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.1">v1.1</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div>
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  <h5 class="panel-title">
                                          <a data-toggle="collapse" data-parent="#version" href="#v1.0">v1.0</a><code class="pull-right"></code>
                                      </h5>
                              </div>
                          </div-->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>技术支持</h5>
                </div>
                <div class="ibox-content">
                    <ol>
                        <c:forEach items="${pageOaNotifyTechnology.list}" var="oaNotify">
                            <li>${fns:abbr(oaNotify.title,50)}         ${fns:getDictLabel(oaNotify.type, 'oa_notify_type', '')}
                                <fmt:formatDate value="${oaNotify.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/></li></br>
                        </c:forEach>
                    </ol>
                    <hr>


                </div>
            </div>
            <!-- <div class="ibox float-e-margins">
               <div class="ibox-title">
                   <h5>联系信息</h5>

               </div>
               <div class="ibox-content">
                   <p><i class="fa fa-send-o"></i> 网址：<a href="http://www.yocity.cn" target="_blank">点我啊</a>
                   </p>

               </div>
           </div>
       </div>-->
         </div>
     </div>
    <div class="row">
        <div class="col-sm-4 ui-sortable">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>JeeSpring 技术特点</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link ui-sortable">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.html#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="index.html#">选项1</a>
                            </li>
                            <li><a href="index.html#">选项2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: block;">
                    <ol>
                    <li>使用目前流行的多种web技术，包括spring boot spring mvc、mybatis。</li>
                    <li>Spring cloud 分布式、微服务、集群、zookeper、nignx。</li>
                    <li>代码生成（前端界面、底层代码、微服务的生成）,根据表生成对应的Entity,Service,Dao,Action,JSP,Redis,APP接口等,。</li>
                    <li>RepositoryORM仓库,提供ORM接口和多种实现,可进行配置实现。</li>
                    <li>RepositoryRedis仓库,提供Redis接口和多种实现,可进行配置实现。可以配置调用单机、redis、云redis对接。</li>
                    <li>RepositoryService仓库,提供Service接口和多种实现,可进行配置实现。可以配置调用dubbo、微服务、基础服务器对接接口和实现。</li>
                    <li>查询过滤器，查询功能自动生成，后台动态拼SQL追加查询条件；支持多种匹配方式（全匹配/模糊查询/包含查询/不匹配查询） </li>
                    <li>系统日志监控，详细记录操作日志，可支持追查表修改日志</li>
                    <li>WebSocket集成：集成在线聊天系统。</li>
                    <li>提供常用工具类封装，日志、缓存、验证、字典、组织机构等，常用标签（taglib），获取当前组织机构、字典等数据。</li>
                    <li>连接池监视：监视当期系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。</li>
                    <li>支持数据库: Mysql,Oracle数据库的支持，但不限于数据库，平台留有其它数据库支持接口等</li>
                    <li>要求JDK1.6+</li>
                </ol>
                </div>
            </div>

        </div>
        <div class="col-sm-4 ui-sortable">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>开源授权</h5>
                </div>
                <div class="ibox-content">
                    <p>JeeSpring是一款开源的快速开发平台</p>
                    <ol>
                        <li>基础功能；</li>
                        <li>Redis、ActiveMQ；</li>
                        <li>源码(带注释)；</li>
                        <li>代码生成器；</li>
                        <li>……</li>
                    </ol>
                    <hr>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>联系信息</h5>
                </div>
                <div class="ibox-content">
                    <p><i class="fa fa-send-o"></i> 网址：<a href="http://www.jeespring.org" target="_blank">http://www.jeespring.org</a>
                    </p>
                    <p><i class="fa fa-qq"></i> QQ：<a href="http://wpa.qq.com/msgrd?v=3&amp;uin=117575171&amp;site=qq&amp;menu=yes" target="_blank">516821420</a>
                    </p>
                    <p><i class="fa fa-weixin"></i> 微信：<a href="javascript:;"></a>
                    </p>
                    <p><i class="fa fa-credit-card"></i> 支付宝：<a href="javascript:;" class="支付宝信息">516821420@qq.com</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>