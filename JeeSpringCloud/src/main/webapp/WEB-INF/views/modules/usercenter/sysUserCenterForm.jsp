<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>用户中心管理</title>
	<meta name="decorator" content="default"/>
			    <%@ include file="/WEB-INF/views/include/head.jsp"%>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
		  return false;
		}
		$(document).ready(function() {
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});
	</script>
</head>
<body>
		<form:form id="inputForm" modelAttribute="sysUserCenter" action="${ctx}/usercenter/sysUserCenter/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">用户编号：</label></td>
					<td class="width-35">
						<form:input path="userId" htmlEscape="false" maxlength="64" class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">用户名称：</label></td>
					<td class="width-35">
						<form:input path="userName" htmlEscape="false" maxlength="64" class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">用户手机号：</label></td>
					<td class="width-35">
						<form:input path="userPhone" htmlEscape="false" maxlength="64" class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">纬度：</label></td>
					<td class="width-35">
						<form:input path="lat" htmlEscape="false" maxlength="30" class="form-control "/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">经度：</label></td>
					<td class="width-35">
						<form:input path="lng" htmlEscape="false" maxlength="30" class="form-control "/>
					</td>
					<td class="width-15 active"><label class="pull-right">城市/地址/IP：</label></td>
					<td class="width-35" >
						<form:input path="city" htmlEscape="false" maxlength="30" class="form-control "/>
						<form:input path="address" htmlEscape="false" maxlength="30" class="form-control "/>
						<form:input path="ip" htmlEscape="false" maxlength="30" class="form-control "/>
					</td>
		  		</tr>
		 	</tbody>
		</table>
	</form:form>
</body>
</html>