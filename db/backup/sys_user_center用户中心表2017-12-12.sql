﻿
CREATE TABLE `sys_user_center` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `user_id` varchar(64) DEFAULT NULL COMMENT '用户编号',
  `user_name` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `user_phone` varchar(64) DEFAULT NULL COMMENT '用户手机号',
  `lat` varchar(30) DEFAULT NULL COMMENT '纬度',
  `lng` varchar(30) DEFAULT NULL COMMENT '经度',
  `create_by` varchar(64) NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户中心表';

  alter table sys_user_center add column city varchar(100) DEFAULT NULL COMMENT '城市';
  alter table sys_user_center add column address varchar(255) DEFAULT NULL COMMENT '地址';
