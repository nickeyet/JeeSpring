﻿
INSERT INTO `sys_menu` (`id`,`parent_id`,`parent_ids`,`name`,`sort`,`href`,`target`,`icon`,`is_show`,`permission`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`) 
VALUES ('15f053b48d004ae48ebe9f18d49f5f05','1','0,1,','用户中心',1122,'','','fa-map','1','','1','2017-12-12 09:38:45','1','2017-12-12 09:38:45','','0')
,('45a32d075c854bccb1f89e9a4768733b','15f053b48d004ae48ebe9f18d49f5f05','0,15f053b48d004ae48ebe9f18d49f5f05,','用户中心',30,'/usercenter/sysUserCenter','','','1','','1','2017-12-12 09:39:14','1','2017-12-12 09:39:14','','0')
,('de34a6ee089c41a58962f1ce79df35cc','15f053b48d004ae48ebe9f18d49f5f05','0,15f053b48d004ae48ebe9f18d49f5f05,','用户地图',60,'/../rest/utils/mapapi/getMapUserCenter','','','1','','1','2017-12-12 13:27:19','1','2017-12-12 13:28:09','','0');


CREATE TABLE `sys_user_center` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `user_id` varchar(64) DEFAULT NULL COMMENT '用户编号',
  `user_name` varchar(64) DEFAULT NULL COMMENT '用户名称',
  `user_phone` varchar(64) DEFAULT NULL COMMENT '用户手机号',
  `lat` varchar(30) DEFAULT NULL COMMENT '纬度',
  `lng` varchar(30) DEFAULT NULL COMMENT '经度',
  `create_by` varchar(64) NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户中心表';

  alter table sys_user_center add column city varchar(100) DEFAULT NULL COMMENT '城市';
  alter table sys_user_center add column address varchar(255) DEFAULT NULL COMMENT '地址';

  alter table sys_dict add column picture varchar(255) COMMENT '图片';
  
CREATE TABLE `sys_config` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `type` varchar(100) NOT NULL COMMENT '类型',
  `value` varchar(100) NOT NULL COMMENT '数据值',
  `label` varchar(100) NOT NULL COMMENT '标签名',
  `description` varchar(2000) NOT NULL COMMENT '描述',
  `sort` decimal(10,0) NOT NULL COMMENT '排序（升序）',
  `parent_id` varchar(64) DEFAULT '0' COMMENT '父级编号',
  `parent_ids` varchar(2000) DEFAULT '0' COMMENT '所有父级编号',
  `create_by` varchar(64) NOT NULL COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) NOT NULL COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统配置表';

INSERT INTO `sys_config` VALUES 
('9a9d64ff20124da191cd8c717acfbb07','toExceptionMailAddr','516821420@qq.com','异常接收邮箱地址(可以用逗号隔开发多个邮件)','异常接收邮箱地址(可以用逗号隔开发多个邮件)',111,'0','0','1','2017-12-11 11:46:25','1','2017-12-12 16:49:26','异常接收邮箱地址','0');

DROP TABLE IF EXISTS `monitor`;

CREATE TABLE `monitor` (
  
`id` varchar(64) NOT NULL default '' COMMENT '主键',
  
`cpu` varchar(64) default NULL COMMENT 'cpu使用率',
  
`jvm` varchar(64) default NULL COMMENT 'jvm使用率',
  
`ram` varchar(64) default NULL COMMENT '内存使用率',
  
`toemail` varchar(64) default NULL COMMENT '警告通知邮箱',
  
PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='系统监控';


INSERT INTO `monitor` (`id`,`cpu`,`jvm`,`ram`,`toemail`) VALUES 
 ('1','99','99','99','516821420@qq.com');

DROP TABLE IF EXISTS `systemconfig`;
CREATE TABLE `systemconfig` (
  
`id` varchar(64) NOT NULL default '' COMMENT '主键',
  
`smtp` varchar(64) default NULL COMMENT '邮箱服务器地址',
  
`port` varchar(64) default NULL COMMENT '邮箱服务器端口',
  
`mailname` varchar(64) default NULL COMMENT '系统邮箱地址',
  
`mailpassword` varchar(64) default NULL COMMENT '系统邮箱密码',
  
`smsname` varchar(64) default NULL COMMENT '短信用户名',
  
`smspassword` varchar(64) default NULL COMMENT '短信密码',
  
PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='系统配置';

/*!40000 ALTER TABLE `systemconfig` DISABLE KEYS */;

INSERT INTO `systemconfig` (`id`,`smtp`,`port`,`mailname`,`mailpassword`,`smsname`,`smspassword`) VALUES 
 
('1','smtp.qq.com','25','516821420@qq.com','xxxxxxx','110931','xxxxxx');

/*!40000 ALTER TABLE `systemconfig` ENABLE KEYS */;


INSERT INTO `sys_menu` (`id`,`parent_id`,`parent_ids`,`name`,`sort`,`href`,`target`,`icon`,`is_show`,`permission`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`) VALUES 
('f18fac5c4e6145528f3c1d87dbcb37d5','67','0,1,67,','系统监控管理','70','/monitor/info','','','1','','1','2016-02-02 22:49:07','1','2016-02-02 23:15:07','','0'),
('8e01a74a9ca94a26a5263769c354afb9','67','0,1,67,','系统配置','100','/sys/systemConfig','','','1','sys:systemConfig:index','1','2016-02-07 16:25:22','1','2016-02-07 16:25:22','','0');


--
-- Definition of table `test_line_weather_main_city`
--

DROP TABLE IF EXISTS `test_line_weather_main_city`;
CREATE TABLE `test_line_weather_main_city` (
  `id` varchar(64) NOT NULL default '' COMMENT '主键',
  `create_by` varchar(64) default NULL COMMENT '创建者',
  `create_date` datetime default NULL COMMENT '创建时间',
  `update_by` varchar(64) default NULL COMMENT '更新者',
  `update_date` datetime default NULL COMMENT '更新时间',
  `remarks` varchar(255) character set utf8 default NULL COMMENT '备注信息',
  `del_flag` varchar(64) default NULL COMMENT '逻辑删除标记（0：显示；1：隐藏）',
  `datestr` datetime default NULL COMMENT '日期',
  `beijing_maxtemp` double default NULL COMMENT '北京最高气温',
  `beijing_mintemp` double default NULL COMMENT '北京最低气温',
  `changchun_maxtemp` double default NULL COMMENT '长春最高气温',
  `changchun_mintemp` double default NULL COMMENT '长春最低气温',
  `shenyang_maxtemp` double default NULL COMMENT '沈阳最高气温',
  `shenyang_mintemp` double default NULL COMMENT '沈阳最低气温',
  `haerbin_maxtemp` double default NULL COMMENT '哈尔滨最高气温',
  `haerbin_mintemp` double default NULL COMMENT '哈尔滨最低气温',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='城市气温';


INSERT INTO `test_line_weather_main_city` (`id`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`,`datestr`,`beijing_maxtemp`,`beijing_mintemp`,`changchun_maxtemp`,`changchun_mintemp`,`shenyang_maxtemp`,`shenyang_mintemp`,`haerbin_maxtemp`,`haerbin_mintemp`) VALUES 
 ('a96a31b3648c4be297d0f00ff5599a9f','1','2016-06-02 21:17:18','1','2016-06-22 00:52:34',NULL,'0','2016-06-22 00:00:00',36,18,36,20,16,20,10,8),
 ('ba1a98106bd44a9ebbfd0b90dd3f89e5','1','2016-06-02 21:21:56','1','2016-06-02 21:21:56',NULL,'0','2016-06-30 00:00:00',24,12,36,18,25,24,12,8),
 ('fa1d274c07b744ee870518e79f817ba6','1','2016-06-02 21:16:54','1','2016-06-02 21:16:54',NULL,'0','2016-06-28 00:00:00',32,12,23,10,60,25,10,2);

DROP TABLE IF EXISTS `test_pie_class`;
CREATE TABLE `test_pie_class` (
  `id` varchar(64) NOT NULL default '' COMMENT '主键',
  `create_by` varchar(64) default NULL COMMENT '创建者',
  `create_date` datetime default NULL COMMENT '创建时间',
  `update_by` varchar(64) default NULL COMMENT '更新者',
  `update_date` datetime default NULL COMMENT '更新时间',
  `del_flag` varchar(64) default NULL COMMENT '逻辑删除标记（0：显示；1：隐藏）',
  `class_name` varchar(64) character set utf8 default NULL COMMENT '班级',
  `num` int(11) default NULL COMMENT '人数',
  `remarks` varchar(255) character set utf8 default NULL COMMENT '备注信息',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='班级';


INSERT INTO `test_pie_class` (`id`,`create_by`,`create_date`,`update_by`,`update_date`,`del_flag`,`class_name`,`num`,`remarks`) VALUES 
 ('19141118ea9e46c6b35d8baeb7f3fe94','1','2016-05-26 21:29:26','1','2016-05-26 21:35:06','0','2班',22,'11'),
 ('42b3824ef5dc455e917a3b1f6a8c1108','1','2016-05-26 21:35:26','1','2016-06-02 21:00:10','0','3班',123,''),
 ('49812602ff9445e99219b0d02719dbc1','1','2016-05-26 21:29:33','1','2016-05-26 21:35:00','0','1班',44,'44'),
 ('f5344164632149e199c9c221428f2774','1','2016-06-22 00:52:16','1','2016-06-22 00:52:16','0','4班',4,'4');
 
 alter table gen_table add column pk varchar(100) COMMENT '主键';
 
 INSERT INTO `sys_menu` (`id`,`parent_id`,`parent_ids`,`name`,`sort`,`href`,`target`,`icon`,`is_show`,`permission`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`) VALUES 
 ('0581067c09eb4acbbccb4cf00e05c898','e768001e0fc44005b9ac92a32c96f730','0,79,e768001e0fc44005b9ac92a32c96f730,','折线图','60','/echarts/line','','','1','','1','2016-06-22 00:45:11','1','2016-06-22 00:45:11','','0'),
 ('169219fbf6f641cdb3ff4d1e2c434ddd','e768001e0fc44005b9ac92a32c96f730','0,79,e768001e0fc44005b9ac92a32c96f730,','双数值轴折线图','90','/echarts/linedoublenum','','','1','','1','2016-06-22 00:45:31','1','2016-06-22 00:45:31','','0'),
 ('31c4b44cf1bb40f69b405fce9af481e0','e768001e0fc44005b9ac92a32c96f730','0,79,e768001e0fc44005b9ac92a32c96f730,','玫瑰图','150','/echarts/radar','','','1','','1','2016-06-22 00:46:03','1','2016-06-22 00:46:03','','0'),
 ('56dfdb85ae734bb49926402579d8649a','e768001e0fc44005b9ac92a32c96f730','0,79,e768001e0fc44005b9ac92a32c96f730,','综合报表2','2040','/echarts/chinaWeatherDataBean',NULL,'','1','echarts:chinaWeatherDataBean:list','1','2016-06-22 00:51:39','1','2016-06-22 00:51:39',NULL,'0'),
 ('70e0cd35107f410dabad5a525d642ac6','e768001e0fc44005b9ac92a32c96f730','0,79,e768001e0fc44005b9ac92a32c96f730,','饼图','120','/echarts/pie','','','1','','1','2016-06-22 00:45:47','1','2016-06-22 00:45:47','','0'),
 ('7601161b0f204feea5cb285083ad8c29','56e274e0ec1c41298e19ab46cf4e001f','0,1,56e274e0ec1c41298e19ab46cf4e001f,','短信工具','60','/tools/sms','','fa-commenting-o','1','','1','2016-03-04 00:19:43','1','2016-03-05 10:09:42','','0'),
 ('e4e64e24aa134deaa9d69c3b9495c997','56e274e0ec1c41298e19ab46cf4e001f','0,1,56e274e0ec1c41298e19ab46cf4e001f,','二维码测试','15060','/tools/TwoDimensionCodeController','','fa-qrcode','1','','1','2015-12-10 13:03:43','1','2016-03-05 10:04:53','','0'),
 ('e668f4084dc9446ba32ad64633172ea0','e768001e0fc44005b9ac92a32c96f730','0,79,e768001e0fc44005b9ac92a32c96f730,','柱状图','30','/echarts/bar','','','1','','1','2016-06-22 00:44:47','1','2016-06-22 00:44:47','','0'),
 ('634e27e11ef64840a9ff7cca459599a8','e768001e0fc44005b9ac92a32c96f730','0,1,e768001e0fc44005b9ac92a32c96f730,','综合报表1','2030','/echarts/pieClass',NULL,'','1','echarts:pieClass:list','1','2016-06-22 00:49:31','1','2016-06-22 00:49:31',NULL,'0'),
 ('e768001e0fc44005b9ac92a32c96f730','79','0,79,','统计报表','10','','','fa-line-chart','1','','1','2016-06-22 00:43:08','1','2016-06-22 00:44:22','','0');

 DROP TABLE IF EXISTS `calendar`;
CREATE TABLE `calendar` (
  `id` varchar(64) NOT NULL COMMENT '主键',
  `title` varchar(64) default NULL COMMENT '事件标题',
  `starttime` varchar(64) character set latin1 default NULL COMMENT '事件开始时间',
  `endtime` varchar(64) character set latin1 default NULL COMMENT '事件结束时间',
  `allday` varchar(64) character set latin1 default NULL COMMENT '是否为全天时间',
  `color` varchar(64) character set latin1 default NULL COMMENT '时间的背景色',
  `userid` varchar(64) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日历';

INSERT INTO `calendar` (`id`,`title`,`starttime`,`endtime`,`allday`,`color`,`userid`) VALUES 
 ('343ed3c7486f41298a9bc9df342b8d27','sss','2016-04-27','','1','#f30','1'),
 ('74b3da86093c4d8eb3a95093d6c8b212','333','2016-05-04','','1','#06c','1'),
 ('ac9d77bb842a4e7f9afb55d26e8fd51d','上午开会','2016-04-05 080000','2016-04-05 120000','0','#f30',NULL),
 ('d468d4f2982e409280c1d328d9f3d1c0','一起吃饭','2016-04-21','','1','#360',NULL),
 ('ed8112f26f764301b73ee6671806b6e1','去看电影','2016-04-23','','1','#06c',NULL),
 ('fe29fbbdc50e4d27b266b15fb90f0d41','早上要开会','2016-04-07','2016-04-07','1','#06c',NULL);
 
 INSERT INTO `sys_menu` (`id`,`parent_id`,`parent_ids`,`name`,`sort`,`href`,`target`,`icon`,`is_show`,`permission`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`) VALUES 
('0aa3712414d049a6a24e8bcddeae509a','27','0,1,27,','我的日程','100','/iim/myCalendar','','','1','','1','2016-04-21 21:52:06','1','2016-04-21 21:52:06','','0');


delete from sys_menu where id in('7601161b0f204feea5cb285083ad8c29','f07b7ea555f84116b5390d2a73740817','e46555e269b84e2697857bdbb73f6676','e4e64e24aa134deaa9d69c3b9495c997');
INSERT INTO `sys_menu` (`id`,`parent_id`,`parent_ids`,`name`,`sort`,`href`,`target`,`icon`,`is_show`,`permission`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`) VALUES 
('f07b7ea555f84116b5390d2a73740817','4d463414bc974236941a6a14dc53c73a','0,1,4d463414bc974236941a6a14dc53c73a,','外部邮件','30','/tools/email','','fa-envelope-o','1','','1','2016-03-03 20:39:12','1','2016-04-10 21:21:18','','0'),
('7601161b0f204feea5cb285083ad8c29','4d463414bc974236941a6a14dc53c73a','0,1,4d463414bc974236941a6a14dc53c73a,','短信工具','60','/tools/sms','','fa-commenting-o','1','','1','2016-03-04 00:19:43','1','2016-03-05 10:09:42','','0'),
('e46555e269b84e2697857bdbb73f6676','4d463414bc974236941a6a14dc53c73a','0,1,4d463414bc974236941a6a14dc53c73a,','表单构建器','90','/tools/beautifyhtml','','fa-building-o','1','','1','2016-03-05 10:00:01','1','2016-03-05 10:10:09','','0'),
('e4e64e24aa134deaa9d69c3b9495c997','4d463414bc974236941a6a14dc53c73a','0,1,4d463414bc974236941a6a14dc53c73a,','二维码测试','15060','/tools/TwoDimensionCodeController','','fa-qrcode','1','','1','2015-12-10 13:03:43','1','2016-03-05 10:04:53','','0');

INSERT INTO `sys_menu` (`id`,`parent_id`,`parent_ids`,`name`,`sort`,`href`,`target`,`icon`,`is_show`,`permission`,`create_by`,`create_date`,`update_by`,`update_date`,`remarks`,`del_flag`) VALUES ('52e9584490a6405eaf79c4fb41ac01d7','3c92c17886944d0687e73e286cada573','0,3c92c17886944d0687e73e286cada573,','请假单mvvm',150,'../webmvvm/admin/modules/oa/formLeavemList.html','','','1','','1','2017-12-26 10:59:55','1','2017-12-26 10:59:55','','0');

alter table sys_user_center add column ip varchar(100) COMMENT 'ip';

INSERT INTO `sys_config` VALUES ('20cb1f4466264afb9e7ec7671cc72334','homePageAboveInfomation','true','后台管理首页展示','大众认同、大众参与、成就大众、大众分享的开发平台。',1,'0','0','1','2018-01-08 16:41:11','1','2018-01-09 13:05:30','','0');

INSERT INTO `sys_dict` VALUES ('55cf3c7662a749139169c0c3704956ac','5','技术支持','oa_notify_type','通知 通告类型',50,'0','1','2017-12-20 13:28:08','1','2017-12-20 13:31:12','','0','');

INSERT INTO `sys_dict` VALUES ('440adeb429f249ef860366823972b11a','4','升级日志','oa_notify_type','通知 通告类型',40,'0','1','2017-12-20 13:24:32','1','2017-12-20 13:31:06','','0','');

update sys_menu set name='用户中心大数据' where id='15f053b48d004ae48ebe9f18d49f5f05';


INSERT INTO `sys_menu` VALUES ('f17015d850b74a469bcd280b879318a5','15f053b48d004ae48ebe9f18d49f5f05','0,15f053b48d004ae48ebe9f18d49f5f05,','行为分析大数据',90,'','','','1','','1','2018-01-17 10:01:16','1','2018-01-17 10:01:16','','0');


INSERT INTO `sys_menu` VALUES ('06bbe325954141c6bcd5fab036fd15d7','f17015d850b74a469bcd280b879318a5','0,f17015d850b74a469bcd280b879318a5,','终端统计',210,'https://www.jiguang.cn/stat/#/app/99b55b1cd938c4d486b70f3b/device','_blank','','1','','1','2018-01-17 10:10:51','1','2018-01-17 10:10:51','','0'),('1df39279b41140a299e175bbfc7d2898','f17015d850b74a469bcd280b879318a5','0,f17015d850b74a469bcd280b879318a5,','用户统计',60,'https://www.jiguang.cn/stat/#/app/99b55b1cd938c4d486b70f3b/user','_blank','','1','','1','2018-01-17 10:04:59','1','2018-01-17 10:07:22','','0'),('3fd1e1d1d3404e2ca0c62504b14d81df','f17015d850b74a469bcd280b879318a5','0,f17015d850b74a469bcd280b879318a5,','推送统计',30,'https://www.jiguang.cn/stat/#/app/99b55b1cd938c4d486b70f3b/push','_blank','','1','','1','2018-01-17 10:01:50','1','2018-01-17 10:03:23','','0'),('68a9040b57974bb6bf4d1d576a42ad70','f17015d850b74a469bcd280b879318a5','0,f17015d850b74a469bcd280b879318a5,','账户统计',90,'https://www.jiguang.cn/stat/#/app/99b55b1cd938c4d486b70f3b/account_overview','_blank','','1','','1','2018-01-17 10:07:48','1','2018-01-17 10:07:48','','0'),
('3216cc32f2984fe8aa9759ddeb113a80','f17015d850b74a469bcd280b879318a5','0,f17015d850b74a469bcd280b879318a5,','错误分析',150,'https://www.jiguang.cn/stat/#/app/99b55b1cd938c4d486b70f3b/crash_log_list','_blank','','1','','1','2018-01-17 10:09:30','1','2018-01-17 10:09:30','','0'),('5ad0ae67bc1a475e92556d5b1df95e89','f17015d850b74a469bcd280b879318a5','0,f17015d850b74a469bcd280b879318a5,','数据分析',240,'https://www.jiguang.cn/stat/#/app/99b55b1cd938c4d486b70f3b/vip_rank','_blank','','1','','1','2018-01-17 10:11:20','1','2018-01-17 10:11:20','','0'),
('9e5e93600a144e8eb289ddf02b98afc3','f17015d850b74a469bcd280b879318a5','0,f17015d850b74a469bcd280b879318a5,','事件统计',180,'https://www.jiguang.cn/stat/#/app/99b55b1cd938c4d486b70f3b/event_purchase','_blank','','1','','1','2018-01-17 10:10:07','1','2018-01-17 10:10:07','','0'),
('43c6bd5a99904e3aa340af9f9f0bd1a5','f17015d850b74a469bcd280b879318a5','0,f17015d850b74a469bcd280b879318a5,','参与留存',120,'https://www.jiguang.cn/stat/#/app/99b55b1cd938c4d486b70f3b/retention_online','_blank','','1','','1','2018-01-17 10:08:55','1','2018-01-17 10:08:55','','0');


INSERT INTO `sys_menu` VALUES ('067773a219a54ba083eafffd5edb4c1c','2aff519ec9aa457baa5c87e3bad693a9','0,2aff519ec9aa457baa5c87e3bad693a9,','我的活动',60,'https://hdm.faisco.cn/version2/?hd_r=bXlBY3RpdmU','_blank','','1','','1','2018-01-26 16:04:13','1','2018-01-26 16:04:13','','0'),('2aff519ec9aa457baa5c87e3bad693a9','1','0,1,','用户互动营销平台',1152,'','','fa-hourglass-2','1','','1','2018-01-26 15:39:23','1','2018-01-26 16:01:18','','0'),('765a8b01bc9e407eb4a1bb1c44bae3fc','2aff519ec9aa457baa5c87e3bad693a9','0,2aff519ec9aa457baa5c87e3bad693a9,','操作日志',180,'https://hdm.faisco.cn/version2/?hd_r=c3RhZmZMb2c','_blank','','1','','1','2018-01-26 16:14:39','1','2018-01-26 16:15:24','','0'),('90539991c3244e2da1738168b124adee','2aff519ec9aa457baa5c87e3bad693a9','0,2aff519ec9aa457baa5c87e3bad693a9,','创建活动',30,'https://hdm.faisco.cn/version2/','_blank','','1','','1','2018-01-26 16:02:30','1','2018-01-26 16:02:59','','0'),('9fa4fb746ea84e6cb7178eea45333990','2aff519ec9aa457baa5c87e3bad693a9','0,2aff519ec9aa457baa5c87e3bad693a9,','员工权限',150,'https://hdm.faisco.cn/version2/?hd_r=ZW1wbG95ZWU','_blank','','1','','1','2018-01-26 16:14:23','1','2018-01-26 16:14:23','','0'),('b097d4966d4f476583aec72becc5d544','2aff519ec9aa457baa5c87e3bad693a9','0,2aff519ec9aa457baa5c87e3bad693a9,','核销管理',90,'https://hdm.faisco.cn/version2/?hd_r=Y2F2SW5kZXg','_blank','','1','','1','2018-01-26 16:13:19','1','2018-01-26 16:13:19','','0'),('cc3a32158a1549ceb6201223d4872713','2aff519ec9aa457baa5c87e3bad693a9','0,2aff519ec9aa457baa5c87e3bad693a9,','个人中心',120,'https://hdm.faisco.cn/version2/?hd_r=bXlJbmZv','_blank','','1','','1','2018-01-26 16:13:41','1','2018-01-26 16:13:41','','0');

INSERT INTO `sys_menu` VALUES ('16fe1a132de945efa94127a7a9342e00','2aff519ec9aa457baa5c87e3bad693a9','0,2aff519ec9aa457baa5c87e3bad693a9,','易企秀（微信营销推广专业场景制作平台）',240,'http://www.eqxiu.com','_blank','','1','','1','2018-01-29 09:32:49','1','2018-01-29 09:33:14','','0'),('314ca8e40d0248159f0f27fc0cd3e754','2aff519ec9aa457baa5c87e3bad693a9','0,2aff519ec9aa457baa5c87e3bad693a9,','凡科微传单（微信营销推广专业场景制作平台）',210,'http://cdm.faisco.cn/manage/flyerManage.jsp?fromLogin&fromPageId=0','_blank','','1','','1','2018-01-29 09:31:31','1','2018-01-29 09:34:12','','0');

INSERT INTO `sys_menu` VALUES ('112a59793f49494aa778d20aa4a1961e','1','0,1,','用户活动汇平台',1212,'','','','1','','1','2018-01-29 12:36:49','1','2018-01-29 12:36:49','','0'),('2e7ae92652334a4b8144f48f13a84f40','112a59793f49494aa778d20aa4a1961e','0,112a59793f49494aa778d20aa4a1961e,','合作伙伴库管理',90,'http://www.hdh.im/main/#/app/zhubanfang','_blank','','1','','1','2018-01-29 14:06:35','1','2018-01-29 14:06:35','','0'),('312ac8d63568436f90e51673617cb7d4','112a59793f49494aa778d20aa4a1961e','0,112a59793f49494aa778d20aa4a1961e,','企业信息设置',60,'http://www.hdh.im/main/#/app/huodong','_blank','','1','','1','2018-01-29 14:03:04','1','2018-01-29 14:04:25','','0'),('38dc5e6dda5a443081010a0218ab8271','112a59793f49494aa778d20aa4a1961e','0,112a59793f49494aa778d20aa4a1961e,','活动信息管理',30,'http://www.hdh.im/main/#/app/huodong','_blank','','1','','1','2018-01-29 14:02:02','1','2018-01-29 14:02:02','','0'),('3e82310d7dc94cde991c57873aeedf2f','312ac8d63568436f90e51673617cb7d4','0,312ac8d63568436f90e51673617cb7d4,','企业认证信息',90,'http://www.hdh.im/main/#/app/renzheng','_blank','','1','','1','2018-01-29 14:05:42','1','2018-01-29 14:05:42','','0'),('a0af0a784f04462c8da7d1929f0e61d1','312ac8d63568436f90e51673617cb7d4','0,312ac8d63568436f90e51673617cb7d4,','企业资料设置',120,'http://www.hdh.im/main/#/app/qyinfo','_blank','','1','','1','2018-01-29 14:06:04','1','2018-01-29 14:06:04','','0'),('a31129b2de414e9e810a934974de1039','312ac8d63568436f90e51673617cb7d4','0,312ac8d63568436f90e51673617cb7d4,','管理员管理',60,'http://www.hdh.im/main/#/app/users/','_blank','','1','','1','2018-01-29 14:05:12','1','2018-01-29 14:05:12','','0'),('a397f58c16b54b8c85354e01de4df73e','112a59793f49494aa778d20aa4a1961e','0,112a59793f49494aa778d20aa4a1961e,','问卷调查',120,'http://www.hdh.im/main/#/app/wenjuan','_blank','','1','','1','2018-01-29 14:08:37','1','2018-01-29 14:08:37','','0'),('d809f5f48705412da8c61a3ef1b46908','312ac8d63568436f90e51673617cb7d4','0,312ac8d63568436f90e51673617cb7d4,','部门管理',30,'http://www.hdh.im/main/#/app/depts','_blank','','1','','1','2018-01-29 14:04:55','1','2018-01-29 14:04:55','','0');

alter table sys_config add column  picture varchar(255) COMMENT '图片';

INSERT INTO `sys_menu` VALUES ('dea2295be7264d2b912b2bf0ba0e897b','e29b4561caf345b68ce180a0209c3501','0,e29b4561caf345b68ce180a0209c3501,','极光推送',480,'https://www.jiguang.cn/jpush/#/app/99b55b1cd938c4d486b70f3b/push_history/list?type=notification','_blank','','1','','1','2018-01-05 15:55:53','1','2018-01-17 09:59:35','','0');

INSERT INTO `sys_menu` VALUES ('3ffc6d20890341ce9e0b08f6348d0ad2','d554f524b734479c8f1e87ed8e4b9104','0,1,','现场互动娱乐',30,'http://youyu.weijuju.com/#home-panel-new','_blank','','1','','1','2018-02-05 15:52:25','1','2018-02-05 15:52:25','','0'),('d554f524b734479c8f1e87ed8e4b9104','1','0,1,','现场用户活动平台',1242,'','','','1','','1','2018-02-05 15:51:32','1','2018-02-05 15:52:36','','0');

--2018-04-10
INSERT INTO `sys_dict` VALUES ('5b86fff7db9d46aca6066f488bbeadd3','%Y','年','total_type','统计类型',11,'0','1','2018-04-10 10:28:19','1','2018-04-10 10:28:19','','0','')
,('4c849775ab08416484a106f0bd4354c0','%Y-%m','月','total_type','统计类型',12,'0','1','2018-04-10 10:28:32','1','2018-04-10 10:28:41','','0','')
,('12222e7dc1f449069218064328e4a033','%Y-%m-%d','日','total_type','统计类型',22,'0','1','2018-04-10 10:28:50','1','2018-04-10 10:28:50','','0','')
,('ece54e044d5b4d008fee36d86c1ec971','%Y-%m-%d %H','时','total_type','统计类型',32,'0','1','2018-04-10 10:29:00','1','2018-04-10 10:29:00','','0','')
,('c2a5dc4f07c346c68da30fff704f2c3b','%Y-%m-%d %H:%i','分','total_type','统计类型',42,'0','1','2018-04-10 10:29:09','1','2018-04-10 10:29:09','','0','')
,('88d8ea6f3d2848099b33012e9b520593','%Y-%m-%d %H:%i:%S','秒','total_type','统计类型',52,'0','1','2018-04-10 10:29:20','1','2018-04-10 10:29:20','','0','');